#!/bin/bash
### Manage user accounts.

usage() {
    cat <<EOF
Usage: $0 [list | start | stop | del ] [<user>...]

EOF
}

mpa="/usr/share/mailpile/multipile/mailpile-admin.py"

main() {
    local cmd=$1; shift
    case $cmd in
        list)
            $mpa --list ; ls /host/accounts
            ;;
        start|stop|del)
            local users="$@"
            for user in $users; do
                $cmd $user
            done
            ;;
        *)  usage ;;
    esac
}

start() {
    local user=$1
    [[ -d /home/$user ]] || adduser $user --disabled-password --gecos ''
    $mpa --user $user --start
}

stop() {
    local user=$1
    $mpa --user $user --stop
}

del() {
    local user=$1
    stop $user
    $mpa --user $user --delete --force
    sed -i /var/lib/mailpile/apache/usermap.txt \
        -e "/^$user /d"
    deluser $user
    rm -rf /home/$user
}

# start the script
main "$@"
