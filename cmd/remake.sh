cmd_remake_help() {
    cat <<_EOF
    remake
        Build the image from scratch and make again the container.

_EOF
}

cmd_remake() {
    ds build --no-cache
    ds make
}
